FROM node:alpine

ARG BUILD_DATE
ARG VCS_REF

# Original metadata for cdktf container:

LABEL org.opencontainers.image.title="bdwyertech/cdktf" \
      org.opencontainers.image.version=$VCS_REF \
      org.opencontainers.image.description="Infrastructure as Code with Terraform Cloud Development Kit" \
      org.opencontainers.image.authors="Broadridge - Cloud Platform Engineering <oss@broadridge.com>" \
      org.opencontainers.image.url="https://hub.docker.com/r/bdwyertech/cdktf" \
      org.opencontainers.image.source="https://github.com/bdwyertech/dkr-cdktf.git" \
      org.opencontainers.image.revision=$VCS_REF \
      org.opencontainers.image.created=$BUILD_DATE \
      org.label-schema.name="bdwyertech/cdktf" \
      org.label-schema.description="Infrastructure as Code with Terraform Cloud Development Kit" \
      org.label-schema.url="https://hub.docker.com/r/bdwyertech/cdktf" \
      org.label-schema.vcs-url="https://github.com/bdwyertech/dkr-cdktf.git"\
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.build-date=$BUILD_DATE

# ENV DEFAULT_CDKTF_CLI_VER=0.3.0

# ckdtf-cli version (node) dependency gotchas:
# - cdktf (pypi)
# - jsii (pypi)
# imports/terraform-aws-modules/lambda/aws - as pulled with `cdktf get`
# ( unsure what the above really is -- 
#      seems to contain options not present in cdktf-cdktf-provider-aws/ or imports/aws.
# )

ENV DEFAULT_CDKTF_CLI_VER=0.4.1
# ENV DEFAULT_CDKTF_CLI_VER=0.5.0

RUN yarn global add cdktf-cli@${DEFAULT_CDKTF_CLI_VER}


# ENV DEFAULT_TERRAFORM_VERSION=0.13.5
ENV DEFAULT_TERRAFORM_VERSION=0.15.5

# TODO: Note - python3-dev, build-base needed for to build some python3 dependencies (such as regex)
#       Might be able to save some resources by making this a multi-stage build.

# TODO: Package cdktf-cdktf-provider-aws-* is currently frequently updated, and needs to be in sync
#       with the version of cdktf. So this is all handled with pip3, as a one-time run after the container is created
#       (via pipenv)

# Install Terraform and awscli
RUN apk add --no-cache jq git curl docker-cli unzip libc6-compat python3 python3-dev build-base py3-pip && \
    pip3 install awscli pipenv && \
    AVAILABLE_TERRAFORM_VERSIONS="0.12.29 ${DEFAULT_TERRAFORM_VERSION}" && \
    for VERSION in ${AVAILABLE_TERRAFORM_VERSIONS}; do curl -LOk https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip && \
    mkdir -p /usr/local/bin/tf/versions/${VERSION} && \
    unzip terraform_${VERSION}_linux_amd64.zip -d /usr/local/bin/tf/versions/${VERSION} && \
    ln -s /usr/local/bin/tf/versions/${VERSION}/terraform /usr/local/bin/terraform${VERSION};rm terraform_${VERSION}_linux_amd64.zip;done && \
    ln -s /usr/local/bin/tf/versions/${DEFAULT_TERRAFORM_VERSION}/terraform /usr/local/bin/terraform


# Install Terragrunt and tfswitch:
# Terragrunt install from: https://github.com/cytopia/docker-terragrunt/blob/master/Dockerfile
# ( Downloads latest from TG repo, or $TG_VERSION, if passed )

# Get Terragrunt
ARG TG_VERSION=latest
RUN set -eux \
    && git clone https://github.com/gruntwork-io/terragrunt /terragrunt \
    && cd /terragrunt \
    && if [ "${TG_VERSION}" = "latest" ]; then \
        VERSION="$( git describe --abbrev=0 --tags )"; \
    else \
        VERSION="$( git tag | grep -E "v${TG_VERSION}\.[.0-9]+" | sort -Vu | tail -1 )" ;\
    fi \
    && curl -sS -L \
        https://github.com/gruntwork-io/terragrunt/releases/download/${VERSION}/terragrunt_linux_amd64 \
        -o /usr/bin/terragrunt \
    && chmod +x /usr/bin/terragrunt \
    && curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | sh
    # Get tfswitch


COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]

# RUN adduser cdktf -Dh /home/cdktf
# USER cdktf
# WORKDIR /cdktf
WORKDIR /cdktg

CMD ["cdktf"]
